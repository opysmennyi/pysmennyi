package service.implementation;

import dao.implementation.RegestryNumberDaoImpl;
import model.entitydata.RegestryNumber;
import service.inrterfaces.RegestryNumberServices;

import java.sql.SQLException;
import java.util.List;

public class RegestryNumberServicesImpl implements RegestryNumberServices {
    @Override
    public List<RegestryNumber> findAll() throws SQLException {
        return new RegestryNumberDaoImpl().findAll();
    }

    @Override
    public RegestryNumber findById(Integer regestryNumber) throws SQLException {
        return new RegestryNumberDaoImpl().findById(regestryNumber);
    }

    @Override
    public int create(RegestryNumber regestryNumber) throws SQLException {
        return new RegestryNumberDaoImpl().create(regestryNumber);
    }

    @Override
    public int update(RegestryNumber regestryNumber) throws SQLException {
        return new RegestryNumberDaoImpl().update(regestryNumber);
    }

    @Override
    public int delete(RegestryNumber id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer regestryNumber) throws SQLException {
        return new RegestryNumberDaoImpl().delete(regestryNumber);
    }
}
