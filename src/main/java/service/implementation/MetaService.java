package service.implementation;

import dao.implementation.MetaDaoImpl;
import model.meta.TableMeta;
import service.inrterfaces.MetaServices;

import java.sql.SQLException;
import java.util.List;

public class MetaService implements MetaServices {

    @Override
    public List<TableMeta> getTablesStructure() throws SQLException {
        return new MetaDaoImpl().getTablesStructure();
    }
}
