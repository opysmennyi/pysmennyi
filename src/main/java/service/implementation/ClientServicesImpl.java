package service.implementation;

import dao.implementation.ClientDaoImpl;
import model.entitydata.Client;
import service.inrterfaces.ClientServices;

import java.sql.SQLException;
import java.util.List;

public class ClientServicesImpl implements ClientServices {

    @Override
    public List<Client> findAll() throws SQLException {
        return new ClientDaoImpl().findAll();
    }

    @Override
    public Client findById(Integer id_client) throws SQLException {
        return new ClientDaoImpl().findById(id_client);
    }

    @Override
    public int create(Client client) throws SQLException {
        return new ClientDaoImpl().create(client);
    }

    @Override
    public int update(Client client) throws SQLException {
        return new ClientDaoImpl().update(client);
    }

    @Override
    public int delete(Client id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_client) throws SQLException {
        return new ClientDaoImpl().delete(id_client);
    }
}
