package service.inrterfaces;

import model.entitydata.Agent_has_agency;

import java.sql.SQLException;

public interface AgentHasAgencyServices extends GeneralServices<Agent_has_agency, Integer> {
    int delete(Integer id_agent) throws SQLException;
}
