package service.inrterfaces;


import model.meta.TableMeta;

import java.sql.SQLException;
import java.util.List;

public interface MetaServices {
    List<TableMeta> getTablesStructure() throws SQLException;
}
