package service.inrterfaces;

import model.entitydata.Place;

import java.sql.SQLException;

public interface PlaceServices extends GeneralServices<Place, Integer> {
    int delete(Integer id_place) throws SQLException;
}
