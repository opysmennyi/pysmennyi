package service.inrterfaces;

import model.entitydata.Agency;

import java.sql.SQLException;

public interface AgencyServices extends GeneralServices<Agency, Integer> {
    int delete(Integer id_agency) throws SQLException;
}
