package dao.interfaces;

import model.entitydata.Place;

import java.sql.SQLException;

public interface PlaceDAO extends GeneralDAO<Place, Integer> {
    int delete(Integer id_place) throws SQLException;
}
