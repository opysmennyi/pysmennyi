package dao.interfaces;

import model.entitydata.Client;

import java.sql.SQLException;

public interface ClientDAO extends GeneralDAO<Client, Integer> {
    int delete(Integer id_client) throws SQLException;
}
