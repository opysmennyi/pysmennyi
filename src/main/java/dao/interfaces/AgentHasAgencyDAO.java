package dao.interfaces;

import model.entitydata.Agent_has_agency;

import java.sql.SQLException;

public interface AgentHasAgencyDAO extends GeneralDAO<Agent_has_agency, Integer> {
    int delete(Integer id) throws SQLException;
}
