package dao.interfaces;

import model.entitydata.Agent_has_landlord;

import java.sql.SQLException;

public interface AgentHasLandorDAO extends GeneralDAO<Agent_has_landlord, Integer> {
    int delete(Integer id) throws SQLException;
}
