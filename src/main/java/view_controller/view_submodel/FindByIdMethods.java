package view_controller.view_submodel;

import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;
import view_controller.ConstantView.ConstantsView;

import java.sql.SQLException;

public class FindByIdMethods {
    private static Logger LOG = LogManager.getLogger(FindByIdMethods.class);

    public static void findByIdAgency() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENCYID);
        final Integer id_agency = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        final Agency agency = agencyServices.findById(id_agency);
        LOG.info(agency);
    }

    public static void findByIdAgent() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        final Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final AgentServicesImpl agentServices = new AgentServicesImpl();
        final Agent agent = agentServices.findById(id_agent);
        LOG.info(agent);
    }

    public static void findByIdAHA() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        final Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        final Agent_has_agency agent_has_agency = agentHasAgencyServices.findById(id_agent);
        LOG.info(agent_has_agency);
    }

    public static void findByIdAHL() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        final Integer id_agent = ConstantsView.INPUT.nextInt();
        final AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        final Agent_has_landlord agent_has_landlord = agentHasLandorServices.findById(id_agent);
        LOG.info(agent_has_landlord);
    }

    public static void findByIdClient() throws SQLException {
        LOG.trace(ConstantsView.INPUTCLIENTID);
        final Integer id_client = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final ClientServicesImpl clientServices = new ClientServicesImpl();
        final Client client = clientServices.findById(id_client);
        LOG.info(client);
    }

    public static void findByIdLandlord() throws SQLException {
        LOG.trace(ConstantsView.INPUTLANDLORDID);
        final Integer id_landor = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final LandorServicesImpl landorServices = new LandorServicesImpl();
        final Landlord landlord = landorServices.findById(id_landor);
        LOG.info(landlord);
    }
    public static void findByIdPersonalInfo() throws SQLException {
        LOG.trace(ConstantsView.INPUTPASSPORTID);
        final Integer passport_id = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        final Personal_info personal_info = personalInfoServices.findById(passport_id);
        LOG.info(personal_info);
    }
    public static void findByIdPlace() throws SQLException {
        LOG.trace(ConstantsView.INPUTPLACEID);
        final Integer id_place = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final PlaceServicesImpl placeServices = new PlaceServicesImpl();
        final Place place = placeServices.findById(id_place);
        LOG.info(place);
    }
    public static void findByIdRegestryNumber() throws SQLException {
        LOG.trace(ConstantsView.INPUTREGISTRYNUMBER);
        final Integer regestry_number = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        final RegestryNumber regestryNumber = regestryNumberServices.findById(regestry_number);
        LOG.info(regestryNumber);
    }
}
