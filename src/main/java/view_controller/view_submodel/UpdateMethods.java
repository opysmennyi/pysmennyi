package view_controller.view_submodel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;
import view_controller.ConstantView.ConstantsView;

import java.sql.SQLException;

public class UpdateMethods {
    private static Logger LOG = LogManager.getLogger(UpdateMethods.class);

    public static void updateAgency() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENCYID);
        final AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        final int count = agencyServices.update(CreateUpdateBase.createUpdateAgency());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAgent() throws SQLException {
        final AgentServicesImpl agentServices = new AgentServicesImpl();
        final int count = agentServices.update(CreateUpdateBase.createUpdateAgent());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAHA() throws SQLException {
        final AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        final int count = agentHasAgencyServices.update(CreateUpdateBase.createUpdateAHA());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAHL() throws SQLException {
        final AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        final int count = agentHasLandorServices.update(CreateUpdateBase.createUpdateAHL());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateClient() throws SQLException {
        final ClientServicesImpl clientServices = new ClientServicesImpl();
        final int count = clientServices.update(CreateUpdateBase.createUpdateClient());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateLandlord() throws SQLException {
        final LandorServicesImpl landorServices = new LandorServicesImpl();
        final int count = landorServices.update(CreateUpdateBase.createUpdatelandlord());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateePersonalInfo() throws SQLException {
        final PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        final int count = personalInfoServices.update(CreateUpdateBase.createUpdatePersonalInfo());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updatePlace() throws SQLException {
        final PlaceServicesImpl placeServices = new PlaceServicesImpl();
        final int count = placeServices.update(CreateUpdateBase.createUpdatePlace());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateRegestryNumber() throws SQLException {
        final RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        final int count = regestryNumberServices.update(CreateUpdateBase.createUpdareRegestryNumber());
        LOG.info(ConstantsView.CREATED + count);
    }
}
