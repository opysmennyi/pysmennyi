package view_controller.view_submodel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;
import view_controller.ConstantView.ConstantsView;

import java.sql.SQLException;

public class DeleteMethods {
    private static Logger LOG = LogManager.getLogger(DeleteMethods.class);

    public static void deleteAgency() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENCYID);
        final Integer id_agency = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        final int count = agencyServices.delete(id_agency);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteAgent() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        final Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final AgentServicesImpl agentServices = new AgentServicesImpl();
        final int count = agentServices.delete(id_agent);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteAHA() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        final Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        final int count = agentHasAgencyServices.delete(id_agent);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteAHL() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        final Integer id_agent = ConstantsView.INPUT.nextInt();
        final AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        final int count = agentHasLandorServices.delete(id_agent);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteClient() throws SQLException {
        LOG.trace(ConstantsView.INPUTCLIENTID);
        final Integer id_client = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final ClientServicesImpl clientServices = new ClientServicesImpl();
        final int count = clientServices.delete(id_client);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteLandlord() throws SQLException {
        LOG.trace(ConstantsView.INPUTLANDLORDID);
        final Integer id_landor = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final LandorServicesImpl landorServices = new LandorServicesImpl();
        final int count = landorServices.delete(id_landor);
        LOG.info(ConstantsView.DELETED, count);
    }
    public static void deletePersonalInfo() throws SQLException {
        LOG.trace(ConstantsView.INPUTPASSPORTID);
        final Integer passport_id = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        final int count = personalInfoServices.delete(passport_id);
        LOG.info(ConstantsView.DELETED, count);
    }
    public static void deletePlace() throws SQLException {
        LOG.trace(ConstantsView.INPUTPLACEID);
        final Integer id_place = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final PlaceServicesImpl placeServices = new PlaceServicesImpl();
        final int count = placeServices.delete(id_place);
        LOG.info(ConstantsView.DELETED, count);
    }
    public static void deleteRegestryNumber() throws SQLException {
        LOG.trace(ConstantsView.INPUTREGISTRYNUMBER);
        final Integer regestry_number = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        final RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        final int count = regestryNumberServices.delete(regestry_number);
        LOG.info(ConstantsView.DELETED, count);
    }
}
