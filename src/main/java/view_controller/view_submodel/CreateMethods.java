package view_controller.view_submodel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;
import view_controller.ConstantView.ConstantsView;

import java.sql.SQLException;

public class CreateMethods {

    private static Logger LOG = LogManager.getLogger(CreateMethods.class);

    public static void createAgency() throws SQLException {
        final AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        final int count = agencyServices.create(CreateUpdateBase.createUpdateAgency());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void createAgent() throws SQLException {
        final AgentServicesImpl agentServices = new AgentServicesImpl();
        final int count = agentServices.create(CreateUpdateBase.createUpdateAgent());
        LOG.info(ConstantsView.CREATED + count);

    }
    public static void createAHA() throws SQLException {
        final AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        final int count = agentHasAgencyServices.create(CreateUpdateBase.createUpdateAHA());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void createAHL() throws SQLException {
        final AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        final int count = agentHasLandorServices.create(CreateUpdateBase.createUpdateAHL());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void createClient() throws SQLException {

        final ClientServicesImpl clientServices = new ClientServicesImpl();
        final int count = clientServices.create(CreateUpdateBase.createUpdateClient());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void createLandlord() throws SQLException {
        final LandorServicesImpl landorServices = new LandorServicesImpl();
        final int count = landorServices.create(CreateUpdateBase.createUpdatelandlord());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void createPersonalInfo() throws SQLException {
        final PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        final int count = personalInfoServices.create(CreateUpdateBase.createUpdatePersonalInfo());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void createPlace() throws SQLException {

        final PlaceServicesImpl placeServices = new PlaceServicesImpl();
        final int count = placeServices.create(CreateUpdateBase.createUpdatePlace());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void createRegestryNumber() throws SQLException {

        final RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        final int count = regestryNumberServices.create(CreateUpdateBase.createUpdareRegestryNumber());
        LOG.info(ConstantsView.CREATED + count);
    }
}
