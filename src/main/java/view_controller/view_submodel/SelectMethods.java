package view_controller.view_submodel;

import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;

import java.sql.SQLException;
import java.util.List;

public class SelectMethods {
    private static Logger LOG = LogManager.getLogger(SelectMethods.class);

    public static void selectAgency() throws SQLException {
        LOG.info("\nTableAnnotation: Agency");
        final AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        final List<Agency> agencies = agencyServices.findAll();
        for (final Agency agency : agencies) {
            LOG.trace(agency);
        }
    }

    public static void selectAgent() throws SQLException {
        LOG.info("\nTableAnnotation: Agent");
        final AgentServicesImpl agentServices = new AgentServicesImpl();
        final List<Agent> agents = agentServices.findAll();
        for (final Agent agent : agents) {
            LOG.trace(agent);
        }
    }


    public static void selectAHA() throws SQLException {
        LOG.info("\nTableAnnotation: Agent has agency");
        final AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        final List<Agent_has_agency> agent_has_agencies = agentHasAgencyServices.findAll();
        for (final Agent_has_agency agent_has_agency : agent_has_agencies) {
            LOG.trace(agent_has_agency);
        }
    }

    public static void selectAHL() throws SQLException {
        LOG.info("\nTableAnnotation: Agent has landlord");
        final AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        final List<Agent_has_landlord> agent_has_landlords = agentHasLandorServices.findAll();
        for (final Agent_has_landlord agent_has_landlord : agent_has_landlords) {
            LOG.trace(agent_has_landlord);
        }
    }

    public static void selectClient() throws SQLException {
        LOG.info("\nTableAnnotation: Client");
        final ClientServicesImpl clientServices = new ClientServicesImpl();
        final List<Client> clients = clientServices.findAll();
        for (final Client client : clients) {
            LOG.trace(client);
        }
    }
    public static void selectLandlord() throws SQLException {
        LOG.info("\nTableAnnotation: Landlordr");
        final LandorServicesImpl landorServices = new LandorServicesImpl();
        final List<Landlord> landlords = landorServices.findAll();
        for (final Landlord landlord : landlords) {
            LOG.trace(landlord);
        }
    }
    public static void selectPersonalInfo() throws SQLException {
        LOG.info("\nTableAnnotation: Personal info");
        final PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        final List<Personal_info> personal_infos = personalInfoServices.findAll();
        for (final Personal_info personal_info : personal_infos) {
            LOG.trace(personal_info);
        }
    }
    public static void selectPlace() throws SQLException {
        LOG.info("\nTableAnnotation: Place");
        final PlaceServicesImpl placeServices = new PlaceServicesImpl();
        final List<Place> places = placeServices.findAll();
        for (final Place place : places) {
            LOG.trace(place);
        }
    }
    public static void selectRegestryNumber() throws SQLException {
        LOG.info("\nTableAnnotation: Registry number");
        final RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        final List<RegestryNumber> regestryNumbers = regestryNumberServices.findAll();
        for (final RegestryNumber regestryNumber : regestryNumbers) {
            LOG.trace(regestryNumber);
        }
    }
    public static void selectAllTable() throws SQLException {
        selectAgency();
        selectAgent();
        selectAHA();
        selectAHL();
        selectClient();
        selectLandlord();
        selectPersonalInfo();
        selectPlace();
        selectRegestryNumber();
    }
}
