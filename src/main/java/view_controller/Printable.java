package view_controller;

import java.io.IOException;
import java.sql.SQLException;

@FunctionalInterface
public interface Printable {
    void print() throws SQLException, IOException;
}
