package persistant.constant_connection;

public class ConstantConnection {
    public static final String URL_KEY = "url";
    public static final String USER_KEY = "user";
    public static final String PASSWORD_KEY = "password";
}
