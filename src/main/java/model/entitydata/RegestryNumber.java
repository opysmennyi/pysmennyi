package model.entitydata;

import model.annotation.ColumnAnnotation;
import model.annotation.PrimaryKeyAnnotation;
import model.annotation.TableAnnotation;

@TableAnnotation(name = "regestry_number")
public class RegestryNumber {
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "regestry_number")
    private String regestry_number;

    public RegestryNumber() {
    }

    public RegestryNumber(String regestry_number) {
        this.regestry_number = regestry_number;
    }

    public String getRegestry_number() {
        return regestry_number;
    }

    public void setRegestry_number(String regestry_number) {
        this.regestry_number = regestry_number;
    }

    @Override
    public String toString() {
        return String.format("%-15s", regestry_number);
    }
}
