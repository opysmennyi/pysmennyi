package model.properties;

import model.constants_model.ConstantsModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import persistant.persistant_implementation.ConnectionManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public class Property {
    public static Logger LOG = LogManager.getLogger(ConnectionManager.class);

    public static String getProperty(String keyToFile) {
        final Properties properties = new Properties();
        try (InputStream input = Property.class.getClassLoader().getResourceAsStream(ConstantsModel.DBPROPERTIESNAME)) {
            properties.load(Objects.requireNonNull(input));
            return properties.getProperty(keyToFile);
        } catch (IOException e) {
            LOG.error(e);
            e.printStackTrace();
            return null;
        }
    }
}
